//
//  ImageService.swift
//  ImageGallery
//
//  Created by ALI MALA on 2021-11-20.
//

import Foundation
import UIKit
protocol ImageService: class {
    func fetchImage(url: URL, completion: @escaping (UIImage) -> Void)
}

private let remoteImagePresenterCache: NSCache<NSURL, UIImage> = {
    let imageCache = NSCache<NSURL, UIImage>()
    return imageCache
}()

extension ServiceLayerClient: ImageService {
    func fetchImage(url: URL, completion: @escaping (UIImage) -> Void) {
        if let cachedImage = remoteImagePresenterCache.object(forKey: url as NSURL) {
            completion(cachedImage)
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, _, _) in
            if let data = data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    remoteImagePresenterCache.setObject(image, forKey: url as NSURL)
                    completion(image)
                }
            }
        }.resume()
    }
}


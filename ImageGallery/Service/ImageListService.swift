//
//  ImageListService.swift
//  ImageGallery
//
//  Created by ALI MALA on 2021-11-20.
//

import Foundation

protocol ImageListService {
    func fetchImages(searchText: String?, url: String?, completion: @escaping (Result<ImageMetaData, ServiceLayerClient.ServiceError>) -> Void)
}

extension ServiceLayerClient: ImageListService {
    
    func fetchImages(searchText: String?, url: String?, completion: @escaping (Result<ImageMetaData, ServiceError>) -> Void) {
        var urlConstructed: URL?
        if let url = url {
            urlConstructed = URL(string: url)
        } else {
            let queryItems: [URLQueryItem] = [
                URLQueryItem(name: "query", value: searchText),
                URLQueryItem(name: "per_page", value: "30"),
            ]
            var components = URLComponents(
                url: URL(string: Endpoint.baseURL.path)!.appendingPathComponent(Endpoint.searchImages.path),
                resolvingAgainstBaseURL: false
            ) ?? URLComponents()
            components.queryItems = queryItems
            urlConstructed = components.url
        }
        createDataTask(url: urlConstructed!) { result in
            completion(result)
        }
    }
}


//
//  Endpoint.swift
//  ImageGallery
//
//  Created by ALI MALA on 2021-11-20.
//

import Foundation

import Foundation

enum Endpoint {
    case baseURL
    case searchImages
    
    var path: String {
        
        switch self {
            case .baseURL:
                return "https://api.pexels.com/"
            case .searchImages:
                return "v1/search"
        }
    }
    
    var APIKey: String {
        return "563492ad6f917000010000011b41a6a603c84e53bbd2b11e0a275bd0"
    }
}

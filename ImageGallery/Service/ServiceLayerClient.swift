//
//  ServiceLayerClient.swift
//  ImageGallery
//
//  Created by ALI MALA on 2021-11-20.
//

import Foundation

// Generic network layer
class ServiceLayerClient {
    
    enum ServiceError: Error {
        case notValid
        case networkError
        case malformedJSON
        case unknown
        case badInput
        case notFound
        case url
        case timedOut
        case noInternet
    }
    
    enum HTTPMethod: String {
        case get = "GET"
        case post = "POST"
        case put = "PUT"
    }
    
    func createDataTask<OutputModel: Decodable>(url: URL, httpMethod: HTTPMethod = .get, completion: @escaping (Result<OutputModel, ServiceError>) -> Void) {
        var request = URLRequest(url: url)
        request.setValue(Endpoint.baseURL.APIKey, forHTTPHeaderField: "Authorization")
        request.httpMethod = httpMethod.rawValue
        
        URLSession.shared.dataTask(with: request) { [unowned self ] (data: Data?, response: URLResponse?, error: Error?) in
            let result: Result<OutputModel, ServiceError> = self.mapToResultType(data, response: response, error: error)
            // move the response from background to main thread
            DispatchQueue.main.async {
                completion(result)
            }
            
        }.resume()
    }
    
    private func mapToResultType<A>(_ data: Data?, response: URLResponse?, error: Error?) -> Result<A, ServiceError> where A: Decodable {
        
        guard let httpResponse = response as? HTTPURLResponse, let data = data else {
            return .failure(ServiceError.unknown)
        }
        
        //can make this exhaustive by doing all the checks
        if let error = error {
            let code = (error as NSError).code
            switch code {
                case NSURLErrorTimedOut:
                    return .failure(ServiceError.timedOut)
                case NSURLErrorNotConnectedToInternet, NSURLErrorNetworkConnectionLost, NSURLErrorCannotConnectToHost:
                    return .failure(ServiceError.noInternet)
                default:
                    print(code)
                    return .failure(ServiceError.unknown)
            }
        }
        
        switch httpResponse.statusCode {
            case 200 ..< 300:
                do {
                    let decoder = JSONDecoder()
                   // decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let data = try decoder.decode(A.self, from: data)
                    return .success(data)
                } catch (let error){
                    print("error for url: \(String(describing: response?.url?.absoluteString)) and error: \(error)")
                    return .failure(ServiceError.malformedJSON)
                }
            default:
                return mapErrorResponse(httpResponse)
        }
    }
    
    private func mapErrorResponse<A>(_ response:HTTPURLResponse) -> Result<A, ServiceError> where A: Decodable {
        
        print("error for url: \(String(describing: response.url?.absoluteString))")
        
        switch response.statusCode {
            case 400:
                return .failure(ServiceError.badInput)
            case 404:
                return .failure(ServiceError.notFound)
            default:
                print(response.statusCode)
                return .failure(ServiceError.unknown)
        }
    }
}




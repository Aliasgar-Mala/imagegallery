//
//  EmptyView.swift
//  ImageGallery
//
//  Created by ALI MALA on 2021-11-20.
//

import Foundation
import UIKit

class EmptyView: UIView {
    private let titleLabel = UILabel()
    
    init() {
        super.init(frame: CGRect.zero)
        postInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        postInit()
    }
    
    private func postInit() {
        configureView()
        setupConstraints()
    }
    
    private func configureView() {
        translatesAutoresizingMaskIntoConstraints = false
        tintColor = .black
        backgroundColor = .white
        layoutMargins = .zero
        accessibilityViewIsModal = true
        
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = "Enter something to get started"
        addSubview(titleLabel)
    }
    
    private func setupConstraints() {
        titleLabel.pinToEdges(of: self)
        titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
}



//
//  ViewController.swift
//  ImageGallery
//
//  Created by ALI MALA on 2021-11-20.
//

import UIKit
import QuickLook
// https://stackoverflow.com/questions/30226835/displaying-search-bar-in-navigation-bar-in-ios-8

class ViewController: UIViewController {
    
    private let viewModel: ViewModel
    private let emptyView = EmptyView()
    private let itemsPerRow: CGFloat = 3
    private let closeButton = UIButton(type: .close)
    private let newImageView = UIImageView()
    private let searchController = UISearchController(searchResultsController: nil)

    private let sectionInsets = UIEdgeInsets(
        top: 50.0,
        left: 20.0,
        bottom: 50.0,
        right: 20.0)

    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: view.frame, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    init(viewModel: ViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func setupSearchBar() {
        title = "Search"
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "search Image"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    fileprivate func setupViews() {
        setupSearchBar()
        
        view.addSubview(emptyView)
        emptyView.pinToEdges(of: self.view)
        
        self.view.addSubview(self.collectionView)
        self.collectionView.pinToEdges(of: self.view)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(ImageCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
        setupViews()
        configureBindings()
    }
    
    fileprivate func configureBindings() {
        viewModel.state.bind {[weak self] state in
            guard let self = self else { return }
            switch state {
                case .empty:
                    self.emptyView.isHidden = false
                    self.view.bringSubviewToFront(self.emptyView)
                case .loading:
                    self.emptyView.isHidden = true
                    self.view.showActivityIndicator()
                case .results:
                    self.view.hideActivityIndicator()
                    self.collectionView.reloadData()
                case .error:
                    self.view.hideActivityIndicator()
            }
        }
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ImageCollectionViewCell {
            let data = viewModel.photos[indexPath.item]
            cell.bind(viewModel: data)
            return cell
        }
        
        fatalError("Could not find the cell, is it registered ??")
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.viewModel.photos.count - 1 {
            self.viewModel.nextPageIndicator.value = ()
        }
    }
    // should be extracted in new file as a separate view
    fileprivate func setupFullScreenImage(_ image: UIImage?) {
        self.newImageView.image = image
        self.newImageView.translatesAutoresizingMaskIntoConstraints = false
        self.newImageView.backgroundColor = .white
        self.newImageView.contentMode = .scaleAspectFit
        self.newImageView.isUserInteractionEnabled = true
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.newImageView.startZooming))
        self.newImageView.addGestureRecognizer(pinch)
        self.newImageView.contentMode = .scaleAspectFit
        self.newImageView.enableZoom()
        self.view.addSubview(self.newImageView)
        self.newImageView.pinToEdges(of: self.view)
        
        self.closeButton.translatesAutoresizingMaskIntoConstraints = false
        self.closeButton.addTarget(self, action: #selector(self.dismissFullscreenImage), for: .touchUpInside)
        self.newImageView.addSubview(self.closeButton)
        self.newImageView.bringSubviewToFront(self.closeButton)
        self.closeButton.widthAnchor.constraint(equalToConstant: 19).isActive = true
        self.closeButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.closeButton.trailingAnchor.constraint(equalTo: self.newImageView.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        self.closeButton.topAnchor.constraint(equalTo: self.newImageView.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        viewModel.photos[indexPath.item].fullSizeImageRequest.value = ()
        viewModel.photos[indexPath.item].fullSize.bind { [weak self] image in
            guard let self = self else { return }
            self.navigationController?.isNavigationBarHidden = true
            self.setupFullScreenImage(image)
        }
    }
    
    @objc func dismissFullscreenImage(_ sender: AnyObject) {
        
        self.closeButton.removeFromSuperview()
        self.newImageView.removeFromSuperview()
        self.navigationController?.isNavigationBarHidden = false
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {

        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow

        return CGSize(width: widthPerItem, height: widthPerItem)
    }

    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAt section: Int
    ) -> UIEdgeInsets {
        return sectionInsets
    }

    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
        return sectionInsets.left
    }
}

extension ViewController: UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else {
            return
        }
        viewModel.searchText.value = searchText
    }
}


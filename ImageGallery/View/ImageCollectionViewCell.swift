//
//  ImageCollectionViewCell.swift
//  ImageGallery
//
//  Created by ALI MALA on 2021-11-20.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    let imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(imageView)
        imageView.pinToEdges(of: contentView)
        imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor).isActive = true
    }
    
    func bind(viewModel: ImageCollectionViewModel) {
        viewModel.thumbnail.bind { [weak self] in
            self?.imageView.image = $0
        }
    }
}

//
//  ImageCollectionViewModel.swift
//  ImageGallery
//
//  Created by ALI MALA on 2021-11-20.
//

import UIKit

class ImageCollectionViewModel {
    private let photo: Photo
    private let imageService: ImageService
    
    // input
    let fullSizeImageRequest: Box<()?> = Box(nil)
    
    // output
    let thumbnail: Box<UIImage?> = Box(nil)
    let fullSize: Box<UIImage?> = Box(nil)

    init(photo: Photo, imageService: ImageService) {
        self.photo = photo
        self.imageService = imageService
        downloadThumbnail()
        
        fullSizeImageRequest.bind { request in
            guard let _ = request else {
                return
            }
            
            let placeHolderImage = UIImage(named: "Image")
            self.fullSize.value = placeHolderImage
            if let url = URL(string: photo.source.portrait) {
                imageService.fetchImage(url: url) { [weak self] image in
                    guard let self = self else {return}
                    self.fullSize.value = image
                }
            }
        }
    }
    
    //loading asynchronously
    fileprivate func downloadThumbnail() {
        let placeHolderImage = UIImage(named: "Image")
        thumbnail.value = placeHolderImage
        if let url = URL(string: photo.source.small) {
            imageService.fetchImage(url: url) { [weak self] image in
                guard let self = self else {return}
                self.thumbnail.value = image
            }
        }
    }
}

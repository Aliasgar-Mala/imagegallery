//
//  SearchImageViewModel.swift
//  ImageGallery
//
//  Created by ALI MALA on 2021-11-20.
//

import Foundation
// screen can only be in one of these states
enum ViewModelState {
    case empty
    case loading
    case results
    case error(_ error: Swift.Error)
}

class ViewModel {
    
    // Input
    let searchText: Box<String?> = Box(nil)
    let nextPageIndicator: Box<()> = Box(())
    
    // Output
    let state: Box<ViewModelState> = Box(.empty)
    var photos: [ImageCollectionViewModel] = []
    // private
    private let imageListService: ImageListService
    private let imageService: ImageService
    var imageMetadata: ImageMetaData? = nil

    
    init(imageListService: ImageListService, imageService: ImageService) {
        self.imageListService = imageListService
        self.imageService = imageService
        bind()
    }
    
    fileprivate func mapResults(newQuery: Bool, result: Result<ImageMetaData, ServiceLayerClient.ServiceError>) {
        self.state.value = .loading
        switch result {
            case .success(let imageMetaData):
                self.imageMetadata = imageMetaData
                
                if newQuery {
                    self.photos = imageMetaData.photos.map {
                        ImageCollectionViewModel(photo: $0, imageService: self.imageService)
                    }
                } else {
                    self.photos = self.photos + imageMetaData.photos.map {
                        ImageCollectionViewModel(photo: $0, imageService: self.imageService)
                    }
                }
                self.state.value = self.photos.isEmpty ? .empty : .results
            case .failure(let error):
                self.state.value = .error(error)
        }
    }
    
    private func bind() {
        
        nextPageIndicator.bind { [weak self] in
            guard let self = self else { return }
            guard let url = self.imageMetadata?.nextPage else { return }
            self.imageListService.fetchImages(searchText: nil, url: url) {
                self.mapResults(newQuery: false, result: $0)
            }
        }
        
        searchText.bind { [weak self] in
            guard let self = self else { return }
            guard let searchTerm = $0, !searchTerm.isEmpty else {
                self.state.value = .empty
                return
            }
            self.imageListService.fetchImages(searchText: searchTerm, url: nil) {
                self.mapResults(newQuery: true, result: $0)
            }
        }
    }
}

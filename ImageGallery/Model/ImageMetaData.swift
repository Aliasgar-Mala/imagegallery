//
//  ImageMetaData.swift
//  ImageGallery
//
//  Created by ALI MALA on 2021-11-20.
//

import Foundation

struct ImageMetaData: Decodable {
    let totalResults, page, perPage: Int
    let photos: [Photo]
    let nextPage: String?
    
    enum CodingKeys: String, CodingKey {
        case totalResults = "total_results"
        case page
        case perPage = "per_page"
        case photos
        case nextPage = "next_page"
    }
}

// MARK: - Photo
struct Photo: Decodable {
    let id, width, height: Int
    let url: String
    let photographer: String
    let source: Source
    
    enum CodingKeys: String, CodingKey {
        case id, width, height, url, photographer
        case source = "src"
    }
}

// MARK: - Source
struct Source: Decodable {
    let original, large2X, large, medium: String
    let small, portrait, landscape, tiny: String
    
    enum CodingKeys: String, CodingKey {
        case original
        case large2X = "large2x"
        case large, medium, small, portrait, landscape, tiny
    }
}


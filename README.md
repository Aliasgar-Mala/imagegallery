# README #

### What is this repository for ###

* This is an simple yet exciting app that gives you a list of images as you search

### How do I get set up ###

* Configuration: I have made this app with iOS target 13.5 which is running on Swift 5

### Focus Area ###
* I have focused on architecture and divided my application into layers for modularity and single responsibility principle with the help of MVVM architecture.
* I also prefer doing UI programmatically 

### Dependencies ###
* There's no dependencies required for this app. 
* VM binding with its view is done using a generic Observable called Box. (I have written this generic class referring a lot of articles and talk shows and this is taken from my personal project)
* I have a generic network layer (from my personal project but I have added error handling specifically for this project), 


### Other things of interest ###

* I have used MVVM architecture keeping modularization and scaling in mind and following the SOLID principle. 
* I have done all my views in code, so no xib or storyboard.
* Images are loaded asynchronously on a separate thread for lazy loading and smooth scrolling with caching capabilities.
* API for web calls are defined in Protocol for easier mocking and testing.
* App is primarily build for iPhone for landscape and portrait 

### Future improvement (I would make following changes if I had more time:) ###
* Add Unit test to my view models as the capability is there with the help of protocols
* Cancel the existing request when the new request is fired up for search
* Make full screen Image view as native iOS quicklook item (so there won't be need of imageview and cancel button)
* Make use of master detail desing pattern to support iPad
* Download different quality of images based on wifi and device support

### Who do I talk to? ###

* Please contact me if you have any questions. I can be reached at aliasgar.mala@gmail.com